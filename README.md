# language-pack-kab

Kabyle language pack for Ubuntu.
This is a testing version.

# How to install language-pack-kab-base ?

Download the deb package :

```
wget https://gitlab.com/imsidag/language-pack-kab/-/raw/master/debs/language-pack-kab-base.deb
```

Install the deb package :
```
sudo dpkg -i language-pack-kab-base.deb
```
# How to enable kabyle language ?

After you installed the package, go to Ubuntu *Settings* > *Country and language* > Select *Kabyle* in both *Language* and *Formats* and apply.

Normally, you will be asked to close your session and log in back again.

Enjoy !